//
//  main.m
//  Location
//
//  Created by Mikhail Muzhev on 26/01/2018.
//  Copyright © 2018 Mikhail Muzhev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
