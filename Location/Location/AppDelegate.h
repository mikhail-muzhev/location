//
//  AppDelegate.h
//  Location
//
//  Created by Mikhail Muzhev on 26/01/2018.
//  Copyright © 2018 Mikhail Muzhev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

